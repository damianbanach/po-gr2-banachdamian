import java.time.LocalDate;
import pl.imiajd.banach.Instrument;
import pl.imiajd.banach.Flet;
import pl.imiajd.banach.Fortepian;
import pl.imiajd.banach.Skrzypce;

import java.util.ArrayList;

public class TestInstrumenty {
    public static void main(String[] args) {
        ArrayList<Instrument> orkiestra = new ArrayList();
        orkiestra.add(new Fortepian("Kawai EX-L", LocalDate.of(1999, 5, 30), "dzwięk fortepianu"));
        orkiestra.add(new Flet("Flet basowy Yamaha YFL-B441 II", LocalDate.of(2005, 12, 1), "dzwięk fletu"));
        orkiestra.add(new Skrzypce("Tonarelli", LocalDate.of(2020, 2, 10), "dzwięk skrzypiec"));
        orkiestra.add(new Skrzypce("Silent Violin", LocalDate.of(2019, 2, 12), "dzwięk skrzypiec"));
        orkiestra.add(new Skrzypce("Yamaha", LocalDate.of(2009, 7, 7), "dzwięk skrzypiec"));

        for(Instrument e : orkiestra) {
            System.out.println(e.toString());
        }
    }
}
