import java.time.LocalDate;

public class zadanie1 {
    public static void main(String[] args)
    {
        Osoba[] ludzie = new Osoba[2];
        String[] p_imiona = {"Paweł"};
        LocalDate p_dataUrodzenia = LocalDate.of(2000, 2, 12);
        LocalDate p_dataZatrudnienia = LocalDate.of(2010, 1, 25);
        String[] s_imiona = {"Małgorzata"};
        LocalDate s_dataUrodzenia = LocalDate.of(2000, 5, 25);
        ludzie[0] = new Pracownik("Kowalski", p_imiona, p_dataUrodzenia, true, 5000, p_dataZatrudnienia);
        ludzie[1] = new Student("Nowak", s_imiona, s_dataUrodzenia, false, "matematyka", 5.0);

        for (Osoba p : ludzie) {
            System.out.println(p.getNazwisko() + ": " + p.getOpis());
        }
    }
}

abstract class Osoba
{
    public Osoba(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec) {
        this.nazwisko = nazwisko;
        this.imiona = imiona;
        this.dataUrodzenia = dataUrodzenia;
        this.plec = plec;
    }

    public abstract String getOpis();

    public String getNazwisko() {
        return this.nazwisko;
    }

    public String[] getImiona() {
        return this.imiona;
    }

    public LocalDate getDataUrodzenia() {
        return this.dataUrodzenia;
    }

    public boolean getPlec() {
        return this.plec;
    }

    private String nazwisko;
    private String[] imiona;
    private LocalDate dataUrodzenia;
    private boolean plec;
}

class Pracownik extends Osoba {

    public Pracownik(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, double pobory, LocalDate dataZatrudnienia) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.pobory = pobory;
        this.dataZatrudnienia = dataZatrudnienia;
    }

    public double getPobory() {
        return this.pobory;
    }

    public LocalDate getDataZatrudnienia() {
        return this.dataZatrudnienia;
    }

    public String getOpis() {
        return String.format("pracownik z pensją %.2f zł", this.pobory);
    }

    private double pobory;
    private LocalDate dataZatrudnienia;
}


class Student extends Osoba {

    public Student(String nazwisko, String[] imiona, LocalDate dataUrodzenia, boolean plec, String kierunek, double sredniaOcen) {
        super(nazwisko, imiona, dataUrodzenia, plec);
        this.kierunek = kierunek;
        this.sredniaOcen = sredniaOcen;
    }

    public String getOpis() {
        return "kierunek studiów: " + this.kierunek;
    }

    public double getSredniaOcen() {
        return this.sredniaOcen;
    }

    public void setSredniaOcen(double sredniaOcen) {
        this.sredniaOcen = sredniaOcen;
    }

    private String kierunek;
    private double sredniaOcen;
}