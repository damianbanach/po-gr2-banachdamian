import java.util.Scanner;
import java.util.PriorityQueue;

public class zadanie1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String command = "";
        PriorityQueue<Task> toDoList = new PriorityQueue<>();
        System.out.println("Polecenie: dodaj priorytet opis -> dodaje nowe zadanie");
        System.out.println("Polecenie: nastepne -> usuwa najbardziej pilne zadanie");
        System.out.println("Polecenie: zakoncz -> konczy prace programu");
        while (!command.equals("zakoncz")) {
            System.out.print("Wpisz polecenie:");
            command = in.nextLine();
            if(command.equals("dodaj priorytet opis")) {
                System.out.print("Podaj priorytet: ");
                int priority = in.nextInt();
                System.out.print("Podaj opis: ");
                String description = in.next();
                toDoList.add(new Task(priority, description));
            }
            if(command.equals("wyswietl")) {
                for(Task e : toDoList)
                    System.out.println(e.getPriority() + " - " + e.getDescription());
                if(toDoList.isEmpty())
                    System.out.println("Brak danych na liscie");
            }
            if(command.equals("nastepne")) {
                toDoList.remove();
            }
        }
        System.out.println("Koniec");
    }
}

class Task implements Comparable<Task> {

    public Task(int priority, String description) {
        this.priority = priority;
        this.description = description;
    }

    @Override
    public int compareTo(Task otherTask) {
        if(this.priority > otherTask.priority)
            return 1;
        if(this.priority < otherTask.priority)
            return -1;
        return 0;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPriority() {
        return this.priority;
    }

    public String getDescription() {
        return this.description;
    }

    private int priority;
    private String description;
}