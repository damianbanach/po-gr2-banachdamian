import java.util.*;

public class zadanie3 {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        HashMap<Student, String> studenty = new HashMap<>();
        HashMap<Integer, Student> two = new HashMap<>();
        String command = "";
        System.out.println("Polecenie: dodaj -> dodaje nowego ucznia oraz ocene");
        System.out.println("Polecenie: wyswietl -> wyswietla uczniow z ocena");
        System.out.println("Polecenie: usun -> usuwa ucznia");
        System.out.println("Polecenie: zmien -> zmienia ocene ucznia");
        System.out.println("Polecenie: zakoncz -> konczy prace programu");
        while(!command.equals("zakoncz")) {
            System.out.print("Podaj polecenie: ");
            command = in.next();
            if(command.equals("dodaj")) {
                System.out.print("Podaj nazwisko: ");
                String nazwisko = in.next();
                System.out.print("Podaj imie: ");
                String imie = in.next();
                System.out.print("Podaj ocene: ");
                String grade = in.next();
                Student student = new Student(nazwisko, imie);
                studenty.put(student, grade);
                two.put(student.getId(), student);
            }
            if(command.equals("wyswietl")) {
                Collection ctwo = two.values();
                ArrayList<Student> array = new ArrayList<>();
                for(Object e : ctwo) {
                    array.add((Student) e);
                }
                array.sort(Student::compareTo);
                for(Student e : array) {
                    System.out.println(e.getId() + "| " + e.getNazwisko() + " " + e.getImie() + ": " + studenty.get(e));
                }
            }
            if(command.equals("usun")) {
                System.out.print("Podaj id: ");
                int id = in.nextInt();
                Student student = two.get(id);
                two.remove(id);
                studenty.remove(student);
            }
            if(command.equals("zmien")) {
                System.out.print("Podaj id: ");
                int id = in.nextInt();
                System.out.print("Podaj ocene: ");
                String grade = in.next();
                studenty.replace(two.get(id), grade);
            }
        }
    }
}

class Student implements Comparable<Student> {
    public Student(String nazwisko, String imie) {
        this.nazwisko = nazwisko;
        this.imie = imie;
        this.id = previous_id;
        ++previous_id;
    }

    @Override
    public int compareTo(Student otherStudent) {
        if(this.nazwisko.compareTo(otherStudent.nazwisko) != 0)
            return this.nazwisko.compareTo(otherStudent.nazwisko);
        else {
            if(this.imie.compareTo(otherStudent.imie) != 0)
                return this.imie.compareTo(otherStudent.imie);
            else {
                if(this.id < otherStudent.id)
                    return -1;
                if(this.id > otherStudent.id)
                    return 1;
                return 0;
            }
        }
    }

    public void setNazwisko(String nazwisko) {
        this.nazwisko = nazwisko;
    }

    public void setImie(String imie) {
        this.imie = imie;
    }

    public String getNazwisko() {
        return nazwisko;
    }

    public String getImie() {
        return imie;
    }

    public int getId() {
        return id;
    }

    private String nazwisko;
    private String imie;
    private int id;
    private static int previous_id = 0;
}
