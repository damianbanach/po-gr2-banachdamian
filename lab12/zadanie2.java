import java.util.Scanner;
import java.util.HashMap;

public class zadanie2{
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        HashMap<String, String> students = new HashMap<>();
        String command = "";
        System.out.println("Polecenie: dodaj -> dodaje nowego ucznia oraz ocene");
        System.out.println("Polecenie: wyswietl -> wyswietla uczniow z ocena");
        System.out.println("Polecenie: usun -> usuwa ucznia");
        System.out.println("Polecenie: zmien -> zmienia ocene ucznia");
        System.out.println("Polecenie: zakoncz -> konczy prace programu");
        while(!command.equals("zakoncz")) {
            System.out.print("Podaj polecenie: ");
            command = in.next();
            if(command.equals("dodaj")) {
                System.out.print("Podaj studenta: ");
                String student = in.next();
                System.out.print("Podaj ocene: ");
                String grade = in.next();
                students.put(student, grade);
            }
            if(command.equals("wyswietl")) {
                for(String e : students.keySet())
                    System.out.println(e+": "+students.get(e));
            }
            if(command.equals("usun")) {
                System.out.print("Podaj studenta: ");
                String student = in.next();
                students.remove(student);
            }
            if(command.equals("zmien")) {
                System.out.print("Podaj studenta: ");
                String student = in.next();
                System.out.print("Podaj nowa ocene: ");
                String grade = in.next();
                students.replace(student, grade);
            }
        }
    }
}
