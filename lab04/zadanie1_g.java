import java.util.Scanner;

public class zadanie1_g {
    public static String nice(String str){
        StringBuffer buff = new StringBuffer();
        String string= "";
        for (int i = 0; i <str.length(); i++)
            string += ((int)str.charAt(i));
        for (int i = 0; i < string.length(); i++)
            buff.append(string.charAt(i));
        for (int i = buff.length(); i >= 0; i -= 3)
            if(buff.length() != i)
                buff.insert(i, "'");
        return buff.toString();
    }
    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis, w ktorym zamienie w nim litery na ich wartości w kodzie ASCII oraz rozdziele separatorem ' co 3 cyfry");
        String napis = scan.nextLine();
        System.out.println(nice(napis));
    }
}
