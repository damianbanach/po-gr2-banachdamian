import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zadanie2 {
    public static void main(String [] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj nazwę pliku wraz z rozszerzeniem: ");
        String name = scan.nextLine();
        System.out.println("Podaj jaki znak ma być zliczony: ");
        char znak = scan.next().charAt(0);
        int ile = 0;
        try{
            File plik = new File(name);
            Scanner scan_file = new Scanner(plik);
            while (scan_file.hasNextLine()) {
                String line = scan_file.nextLine();
                for (int i = 0; i < line.length(); i++)
                    if (line.charAt(i) == znak)
                        ile++;
                scan_file.close();
                System.out.println("Znak '" + znak + "' wystąpił :" + ile);
            }
        }
        catch (FileNotFoundException err){
            System.out.println("Błąd otwarcia pliku");
        }
    }
}