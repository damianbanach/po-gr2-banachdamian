import java.util.Scanner;

public class zadanie1_d {
    public static String repeat(String str, int n){
        String polaczenie = "";
        for(int i=0; i<n; i++){
            polaczenie+=str;
        }
        return polaczenie;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis = scan.nextLine();
        System.out.println("Podaj liczbe powtorzen napisu: ");
        int n = scan.nextInt();
        System.out.println("Polaczony napis: "+repeat(napis, n));
    }
}