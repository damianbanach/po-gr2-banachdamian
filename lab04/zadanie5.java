import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class zadanie5{
    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        float wartosc;
        System.out.println("Podaj kapitał początkowy");
        wartosc = scan.nextFloat();
        BigDecimal kapital = BigDecimal.valueOf(wartosc).setScale(2,RoundingMode.HALF_UP);
        System.out.println("Podaj stopę oprocentowania");
        wartosc = scan.nextFloat();
        BigDecimal procent = BigDecimal.valueOf(wartosc).setScale(4,RoundingMode.HALF_UP);
        BigDecimal nowyKapital = kapital;
        System.out.println("Podaj długość okresu oszczędzania w latach");
        int lata = scan.nextInt();
        for(int i = 0; i < lata; i++)
            nowyKapital = ((BigDecimal.valueOf(100)).multiply(procent)).add(nowyKapital).setScale(2,RoundingMode.HALF_UP);
        System.out.println("Kapitał " + kapital + " po " + lata + " latach na lokacie na " + procent + "% wynosi: " + nowyKapital);
    }
}
