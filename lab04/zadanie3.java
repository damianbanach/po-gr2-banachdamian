import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class zadanie3{
    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj nazwę pliku wraz z rozszerzeniem");
        String name = scan.nextLine();
        System.out.println("Podaj jaki wyraz ma być zliczony");
        String wyraz = scan.nextLine();
        int ile = 0;
        try{
            File file = new File(name);
            Scanner scan_file = new Scanner(file);
            while (scan_file.hasNextLine()) {
                String line = scan_file.nextLine();
                for (int i = 0; i < line.length(); i++) {
                    if (line.charAt(i) == wyraz.charAt(0)) {
                        int n = i;
                        int dlu = 0;
                        for (int j = 0; j < wyraz.length(); j++) {
                            if (n >= line.length())
                                break;
                            if (line.charAt(n) == wyraz.charAt(j))
                                dlu++;
                            n++;
                        }
                        if (dlu == wyraz.length())
                            ile++;
                    }
                }
            }
            scan_file.close();
            System.out.println("Znak '" + wyraz + "' wystąpił :" + ile);
        }
        catch (FileNotFoundException err){
            System.out.println("Błąd otwarcia pliku");
        }
    }
}