import java.math.BigInteger;
import java.util.Scanner;

public class zadanie4 {
    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj n, ktore stowrzy szachownice n x n");
        int n = scan.nextInt();
        if(n == 1 || n == 0){
            int ile = 1;
            if(n!=1){
                ile = 0;
            }
            System.out.println("Na szachownicy jest "+ile+" ziarenek");
        }
        else{
            BigInteger ile = new BigInteger("0");
            BigInteger[][] tab =  new BigInteger[n][n];
            for (int i = 0; i < n; i++){
                for(int j = 0; j < n; j++){
                    if(i == 0 && j == 0){
                        tab[i][j] = BigInteger.valueOf(1);
                        ile = ile.add(tab[i][j]);
                        j++;
                    }
                    if(j != 0)
                        tab[i][j] = (BigInteger.valueOf(2)).multiply(tab[i][j - 1]);
                    else
                        tab[i][j]=(BigInteger.valueOf(2)).multiply(tab[i - 1][n -1]);
                    ile = ile.add(tab[i][j]);
                }
            }
            System.out.println("Na szachownicy jest "+ile+" ziarenek");
        }
    }
}
