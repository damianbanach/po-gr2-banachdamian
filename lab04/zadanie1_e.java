import java.util.Scanner;

public class zadanie1_e {
    public static int countSubStr(String str, String subStr){
        int ile=0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i)==subStr.charAt(0)){
                int n = i;
                int dlu = 0;
                for(int j=0; j<subStr.length(); j++, n++){
                    if( n >= str.length()){
                        break;
                    }
                    if(str.charAt(n)==subStr.charAt(j)){
                        dlu++;
                    }
                }
                if(dlu == subStr.length()){
                    ile++;
                }
            }
        }
        return ile;
    }
    public static int[] where(String str, String subStr){
        int ile = countSubStr(str, subStr);
        int[] tab = new int[ile];
        int n = 0;
        for(int i = 0;i < str.length(); i++) {
            if(str.charAt(i) == subStr.charAt(0)) {
                int y = i;
                int dlu = 0;
                for(int j = 0;j < subStr.length(); j++, y++) {
                    if(y >= str.length())
                        break;
                    if(str.charAt(y) == subStr.charAt(j))
                        dlu++;
                }
                if(dlu == subStr.length()) {
                    tab[n] = i;
                    n++;
                }
            }
        }
        return tab;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis nr 1: ");
        String napis1 = scan.nextLine();
        System.out.println("Podaj napis nr 2: ");
        String napis2 = scan.nextLine();
        int[] tab = where(napis1, napis2);
        for(int i=0; i< tab.length;i++){
            System.out.println(tab[i]);
        }
    }
}
