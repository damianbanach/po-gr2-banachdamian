import java.util.Scanner;

public class zadanie1_a {
    public static int countChar(String str, char c){
        int ile=0;
        char znakZnapisu;
        for(int i=0; i<str.length(); i++){
            znakZnapisu = str.charAt(i);
            if(znakZnapisu==c){
                ile++;
            }
        }
        return ile;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis = scan.nextLine();
        System.out.println("Podaj znak, ktory chcesz zliczyc w napisie: ");
        char znak = scan.next().charAt(0);
        System.out.println("Litera "+znak+" wystapila "+countChar(napis, znak)+" razy");
    }
}
