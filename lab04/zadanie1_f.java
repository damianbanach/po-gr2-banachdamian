import java.util.Scanner;

public class zadanie1_f {
    public static String change(String str){
        StringBuffer buff = new StringBuffer();
        for(int i = 0; i < str.length(); i++) {
            if(str.charAt(i) >= 97 && str.charAt(i) <= 122)
                buff.append(Character.toString(str.charAt(i) - 32));
            else if(str.charAt(i) >= 65 && str.charAt(i) <= 90)
                buff.append(Character.toString(str.charAt(i) + 32));
            else buff.append(Character.toString(str.charAt(i)));
        }
        return buff.toString();
    }

    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis, aby zamienic wielkosc liter na przeciwna");
        String str = scan.nextLine();
        System.out.println(change(str));
    }
}