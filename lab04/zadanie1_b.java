import java.util.Scanner;

public class zadanie1_b {
    public static int countSubStr(String str, String subStr){
        int ile=0;
        for(int i=0; i<str.length(); i++){
            if(str.charAt(i)==subStr.charAt(0)){
                int n = i;
                int dlu = 0;
                for(int j=0; j<subStr.length(); j++, n++){
                    if( n >= str.length()){
                        break;
                    }
                    if(str.charAt(n)==subStr.charAt(j)){
                        dlu++;
                    }
                }
                if(dlu == subStr.length()){
                    ile++;
                }
            }
        }
        return ile;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis = scan.nextLine();
        System.out.println("Podaj fragment, ktory chcesz zliczyc w napisie: ");
        String subStr = scan.nextLine();
        System.out.println("Fragment "+subStr+" wystapil "+countSubStr(napis, subStr)+" razy");
    }
}