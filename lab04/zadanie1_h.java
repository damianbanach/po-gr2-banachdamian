import java.util.Scanner;

public class zadanie1_h {
    public static String nice(String str, char znak, int liczba){
        StringBuffer buff = new StringBuffer();
        String string= "";
        for (int i = 0; i <str.length(); i++)
            string += ((int)str.charAt(i));
        for (int i = 0; i < string.length(); i++)
            buff.append(string.charAt(i));
        for (int i = buff.length(); i >= 0; i -= liczba)
            if(buff.length() != i)
                buff.insert(i, znak);
        return buff.toString();
    }

    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis:");
        String napis = scan.nextLine();
        System.out.println("Podaj znak oddzielajacy:");
        char znak = scan.next().charAt(0);
        System.out.println("Podaj liczbe co ile miejsc:");
        int liczba = scan.nextInt();
        System.out.printf(nice(napis, znak, liczba));
    }
}
