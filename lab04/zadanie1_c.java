import java.util.Scanner;

public class zadanie1_c {
    public static String middle(String str){
        if(str.length()%2==0){
            return Character.toString(str.charAt(str.length() / 2 - 1)) + Character.toString(str.charAt(str.length() / 2));
        }
        return Character.toString(str.charAt(str.length() / 2));
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj napis: ");
        String napis = scan.nextLine();
        System.out.println("Srodek wyrazu to: "+middle(napis));
    }
}
