import java.util.ArrayList;

public class zadanie5 {
    public static void reverse(ArrayList<Integer> a){
        ArrayList<Integer> listReversed = new ArrayList<>();
        for (int i = a.size() - 1; i >= 0; i--) {
            listReversed.add(a.get(i));
        }
        System.out.println(listReversed);
    }
    public static void main(String[] args){
        ArrayList<Integer> listA = new ArrayList<>(8);
        for(int i=0; i<8; i++){
            listA.add(i, i+1);
        }
        System.out.println(listA);
        reverse(listA);
    }
}
