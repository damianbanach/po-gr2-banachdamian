import java.util.ArrayList;
import java.util.Collections;

public class zadanie3 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        int c1 = 0, c2 = 0;
        ArrayList<Integer> mergelistAB = new ArrayList<Integer>();

        while(c1 < a.size() || c2 < b.size()) {
            if(c1 < a.size())
                mergelistAB.add((Integer) a.get(c1++));
            if(c2 < b.size())
                mergelistAB.add((Integer) b.get(c2++));
        }
        return mergelistAB;
    }
    public static ArrayList<Integer> mergeSorted(ArrayList<Integer> a, ArrayList<Integer> b){
        Collections.sort(a);
        Collections.sort(b);
        ArrayList<Integer> mergeSortedList = merge(a, b);
        Collections.sort(mergeSortedList);
        return mergeSortedList;
    }

    public static void main(String[] args){
        ArrayList<Integer> listA = new ArrayList<>(5);
        for(int i=0; i<5; i++){
            listA.add(i, i+1);
        }
        System.out.println(listA);
        ArrayList<Integer> listB = new ArrayList<>(8);
        for(int i=0; i<8; i++){
            listB.add(i, i+1);
        }
        System.out.println(listB);
        System.out.println(merge(listA,listB));
        System.out.println(mergeSorted(listA,listB));
    }
}
