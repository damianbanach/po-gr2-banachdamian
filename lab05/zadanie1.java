import java.util.ArrayList;

public class zadanie1 {
    public static ArrayList<Integer> append(ArrayList<Integer> a, ArrayList<Integer> b){
        ArrayList<Integer> listAB = new ArrayList<>();
        listAB.addAll(0,a);
        listAB.addAll(a.size(), b);
        return listAB;
    }
    public static void main(String[] args){
        ArrayList<Integer> listA = new ArrayList<>(8);
        for(int i=0; i<8; i++){
            listA.add(i, i+1);
        }
        System.out.println(listA);
        ArrayList<Integer> listB = new ArrayList<>(4);
        for(int i=0; i<4; i++){
            listB.add(i, i+1);
        }
        System.out.println(listB);
        System.out.println(append(listA,listB));
    }
}
