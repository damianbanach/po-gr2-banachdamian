import java.util.ArrayList;

public class zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b){
        int c1 = 0, c2 = 0;
        ArrayList<Integer> mergelistAB = new ArrayList<Integer>();

        while(c1 < a.size() || c2 < b.size()) {
            if(c1 < a.size())
                mergelistAB.add((Integer) a.get(c1++));
            if(c2 < b.size())
                mergelistAB.add((Integer) b.get(c2++));
        }
        return mergelistAB;
    }
    public static void main(String[] args){
        ArrayList<Integer> listA = new ArrayList<>(3);
        for(int i=0; i<3; i++){
            listA.add(i, i+1);
        }
        System.out.println(listA);
        ArrayList<Integer> listB = new ArrayList<>(7);
        for(int i=0; i<7; i++){
            listB.add(i, i+5);
        }
        System.out.println(listB);
        System.out.println(merge(listA,listB));
    }
}
