import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;
import java.io.File;
import java.io.FileNotFoundException;

public class zadanie3 {
    public static void main(String[] args) throws FileNotFoundException {
        String path = "plik.txt";
        Scanner file = new Scanner(new File(path));
        ArrayList<String> lista = new ArrayList();
        while (file.hasNextLine()) {
            lista.add(file.nextLine());
        }
        System.out.println("Przed posortowaniem:\n");
        for(String e : lista)
            System.out.println(e);
        Collections.sort(lista);
        System.out.println("\nPo posortowaniu:\n");
        for(String e : lista)
            System.out.println(e);
    }
}
