package pl.imiajd.banach;
import java.time.LocalDate;

public class Student extends Osoba implements Cloneable, Comparable{

    public Student(String nazwisko, LocalDate dataUrodzenia, double sredniaOcen) {
        super(nazwisko, dataUrodzenia);
        this.sredniaOcen = sredniaOcen;
    }

    public String toString() {
        String str = super.toString();
        str = str.substring(0, str.length()-1);
        str = str.concat(", ");
        str = str.concat(String.valueOf(this.sredniaOcen));
        str = str.concat("]");
        return str;
    }

    public boolean equals(Object obj) {
        Student obiekt = (Student) obj;
        if(!super.equals(obj))
            return false;
        if(this.sredniaOcen == obiekt.sredniaOcen)
            return true;
        return false;
    }

    public int compareTo(Object obj) {
        if(super.compareTo(obj) > 0)
            return 1;
        if(super.compareTo(obj) < 0)
            return -1;
        Student obiekt = (Student) obj;
        if(this.sredniaOcen > obiekt.sredniaOcen)
            return 1;
        if(this.sredniaOcen < obiekt.sredniaOcen)
            return -1;
        return 0;
    }
    private double sredniaOcen;
}
