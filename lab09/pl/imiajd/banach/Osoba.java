package pl.imiajd.banach;
import java.time.LocalDate;
import java.util.Objects;

public class Osoba implements Cloneable, Comparable {

    public Osoba(String nazwisko, LocalDate dataUrodzenia) {
        this.nazwisko = nazwisko;
        this.dataUrodzenia = dataUrodzenia;
    }

    public String toString() {
        String str = "Osoba[";
        str = str.concat(this.nazwisko);
        str = str.concat(", ");
        str = str.concat(this.dataUrodzenia.toString());
        str = str.concat("]");
        return str;
    }

    public boolean equals(Object obj) {
        Osoba obiekt = (Osoba) obj;
        if(!this.nazwisko.equals(obiekt.nazwisko))
            return false;
        if(!this.dataUrodzenia.equals(obiekt.dataUrodzenia))
            return false;
        return true;
    }

    public int compareTo(Object obj) {
        Osoba obiekt = (Osoba) obj;
        int int_nazwisko = this.nazwisko.compareTo(obiekt.nazwisko);
        if(int_nazwisko > 0)
            return 1;
        if(int_nazwisko < 0)
            return -1;
        int int_dataUrodzenia = this.dataUrodzenia.compareTo(obiekt.dataUrodzenia);
        if(int_dataUrodzenia > 0)
            return 1;
        if(int_dataUrodzenia < 0)
            return -1;
        return 0;
    }

    private String nazwisko;
    protected LocalDate dataUrodzenia;
}
