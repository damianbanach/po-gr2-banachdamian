import pl.imiajd.banach.Osoba;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestOsoba {

    public static void main(String[] args) {
        ArrayList<Osoba> grupa = new ArrayList();
        grupa.add(new Osoba("Andrzej", LocalDate.of(2000, 6, 27)));
        grupa.add(new Osoba("Figiel", LocalDate.of(1995, 1, 1)));
        grupa.add(new Osoba("Waza", LocalDate.of(1990, 1, 1)));
        grupa.add(new Osoba("Mieszko", LocalDate.of(1990, 1, 1)));
        grupa.add(new Osoba("Fugiel", LocalDate.of(2000, 2, 1)));
        System.out.println("Przed posortowaniem:");
        for(Osoba e : grupa)
            System.out.println("\t"+e.toString());
        Collections.sort(grupa);
        System.out.println("Po Posortowaniu:");
        for(Osoba e : grupa)
            System.out.println("\t"+e.toString());
    }
}