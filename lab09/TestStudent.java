import pl.imiajd.banach.Student;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class TestStudent {
    public static void main(String[] args) {
        ArrayList<Student> grupa = new ArrayList();
        grupa.add(new Student("Andrzej", LocalDate.of(1999, 7, 7), 5.0));
        grupa.add(new Student("Figiel", LocalDate.of(1999, 7, 7), 4.5));
        grupa.add(new Student("Waza", LocalDate.of(1999, 7, 7), 3.5));
        grupa.add(new Student("Mieszko", LocalDate.of(1999, 7, 7), 4.5));
        grupa.add(new Student("Fugiel", LocalDate.of(1999, 7, 7), 3.0));
        System.out.println("Przed posortowaniem:");
        for(Student e : grupa)
            System.out.println("\t"+e.toString());
        Collections.sort(grupa);
        System.out.println("Po Posortowaniu:");
        for(Student e : grupa)
            System.out.println("\t"+e.toString());
    }
}
