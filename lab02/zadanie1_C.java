import java.util.Random;
import java.util.Scanner;

public class zadanie1_C {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        int max = 0;
        int ileMax = 0;
        Random number = new Random();
        for(int i=0; i<n; i++){
            tab[i] = number.nextInt(999 - (-999) + 1) + (-999);
        }
        max = tab[0];
        for(int i=0; i<n; i++){
            if(tab[i]>=max){
                max = tab[i];
            }
        }
        for(int i=0; i<n; i++){
            if(tab[i]==max){
                ileMax+=1;
            }
        }
        System.out.println("Element najwiekszy tablicy to: "+max+", ktory wystapil "+ileMax+" razy");
    }
}
