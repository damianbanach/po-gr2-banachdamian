import java.util.Scanner;
import java.util.Random;

public class zadanie2_C {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        Random number = new Random();
        for(int i=0; i < tab.length; i++){
            tab[i] = number.nextInt(maxWartosc - (minWartosc)+1) + (minWartosc);
        }
    }
    public static int ileMaksymalnych(int tab[]){
        int ileMaks = 0;
        int max = 0;
        max = tab[0];
        for(int i=0; i < tab.length; i++){
            if(tab[i]>=max){
                max=tab[i];
            }
        }
        for(int i=0; i<tab.length; i++){
            if(tab[i]==max){
                ileMaks+=1;
            }
        }
        return ileMaks;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        int minWartosc = -999;
        int maxWartosc = 999;
        generuj(tab, n, minWartosc, maxWartosc);
        System.out.println("Element najwiekszy w tablicy wystapil "+ileMaksymalnych(tab)+" razy");
    }
}
