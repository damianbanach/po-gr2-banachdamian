import java.util.Random;
import java.util.Scanner;

public class zadanie3 {
    public static void generuj (int macierz[][], int x, int y, int minWartosc, int maxWartosc) {
        Random number = new Random();
        for (int i = 0; i < x; i++)
            for(int j = 0; j < y; j++) {
                macierz[i][j] = number.nextInt(maxWartosc - (minWartosc) + 1) + (minWartosc);
            }
    }
    public static void wypisz(int macierz[][], int x, int y) {
        for (int i = 0; i < x; i++) {
            for ( int j = 0; j < y; j++)
                System.out.format("%10s ", macierz[i][j]);
            System.out.println("");
        }
        System.out.println("");
    }
    public static int[][] mnozenie_macierzy(int macierz_a[][], int macierz_b[][], int m, int n, int k) {
        int macierz_c[][] = new int[m][k];
        int suma = 0;
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < k; j++) {
                for (int l = 0; l < n; l++) {
                    suma += macierz_a[i][l] * macierz_b[l][j];
                    macierz_c[i][j] = suma;
                    suma = 0;
                }
            }
        }
        return macierz_c;
    }
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj m:");
        int m = 0;
        do {
            m = scan.nextInt();
        }while(m < 1 || 10 < m);
        System.out.println("Podaj n:");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 10 < n);
        System.out.println("Podaj k:");
        int k = 0;
        do {
            k = scan.nextInt();
        }while(k < 1 || 10 < k);
        int macierz_a[][] = new int[m][n];
        int macierz_b[][] = new int[n][k];
        int macierz_c[][] = new int[m][k];
        int minWartosc = -10;
        int maxWartosc = 10;
        generuj(macierz_a, m, n, minWartosc, maxWartosc);
        generuj(macierz_b, n, k, minWartosc, maxWartosc);
        wypisz(macierz_a, m, n);
        wypisz(macierz_b, n, k);
        macierz_c = mnozenie_macierzy(macierz_a, macierz_b, m, n, k);
        wypisz(macierz_c, m, k);
    }
}
