import java.util.Random;
import java.util.Scanner;

public class zadanie1_F {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        Random number = new Random();
        for(int i=0; i<n; i++){
            tab[i] = number.nextInt(999 - (-999) + 1) + (-999);
            if(tab[i]>=0){
                tab[i]=1;
            }
            else{
                tab[i]=-1;
            }
        }
        for(int i=0; i<n; i++){
            System.out.println(tab[i]);
        }
    }
}

