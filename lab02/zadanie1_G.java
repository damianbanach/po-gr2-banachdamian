import java.util.Scanner;
import java.util.Random;

public class zadanie1_G {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int lewy = 0;
        do {
            lewy = scan.nextInt();
        }while(0 > lewy || lewy > n);
        int prawy = 0;
        do {
            prawy = scan.nextInt();
        }while(0 > prawy || prawy > n);
        int tab1[] = new int[n];
        int tab2[] = new int[prawy - lewy + 1];
        Random number = new Random();
        for(int i = 0; i < tab1.length; i++)
            tab1[i] = number.nextInt(999 - (-999) + 1) + (-999);
        System.arraycopy(tab1, lewy - 1, tab2, 0, tab2.length);
        for(int i = 0; i < tab2.length / 2; i++) {
            int temp = tab2[i];
            tab2[i] = tab2[tab2.length - 1 - i];
            tab2[tab2.length - 1 - i] = temp;
        }
        for(int i = 0; i < tab1.length; i++)
            System.out.println(tab1[i]);
        System.out.println("");
        System.arraycopy(tab2, 0, tab1, lewy - 1, tab2.length);
        for(int i = 0; i < tab1.length; i++)
            System.out.println(tab1[i]);
    }
}
