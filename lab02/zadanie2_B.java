import java.util.Scanner;
import java.util.Random;

public class zadanie2_B {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        Random number = new Random();
        for(int i=0; i < tab.length; i++){
            tab[i] = number.nextInt(maxWartosc - (minWartosc)+1) + (minWartosc);
        }
    }
    public static int ileDodatnich(int tab[]){
        int ileD = 0;
        for(int i=0; i < tab.length; i++){
            if(tab[i]>0){
                ileD+=1;
            }
        }
        return ileD;
    }
    public static int ileZerowych(int tab[]){
        int ileZ = 0;
        for(int i=0; i < tab.length; i++){
            if(tab[i]==0){
                ileZ+=1;
            }
        }
        return ileZ;
    }
    public static int ileUjemnych(int tab[]){
        int ileU = 0;
        for(int i=0; i < tab.length; i++){
            if(tab[i]<0){
                ileU+=1;
            }
        }
        return ileU;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        int minWartosc = -999;
        int maxWartosc = 999;
        generuj(tab, n, minWartosc, maxWartosc);
        System.out.println("Liczb Dodatnich jest: "+ileDodatnich(tab));
        System.out.println("Liczb Zerowych jest: "+ileZerowych(tab));
        System.out.println("Liczb Ujemnych jest: "+ileUjemnych(tab));
    }
}

