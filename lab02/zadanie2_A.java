import java.util.Scanner;
import java.util.Random;
import static java.lang.Math.abs;

public class zadanie2_A {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        Random number = new Random();
        for(int i=0; i < tab.length; i++){
            tab[i] = number.nextInt(maxWartosc - (minWartosc)+1) + (minWartosc);
        }
    }
    public static int ileNieparzystych(int tab[]){
        int ileNP = 0;
        for(int i=0; i < tab.length; i++){
            if(Math.abs(tab[i])%2==1){
                ileNP+=1;
            }
        }
        return ileNP;
    }
    public static int ileParzystych(int tab[]){
        int ileP = 0;
        for(int i=0; i < tab.length; i++){
            if(Math.abs(tab[i])%2==0){
                ileP+=1;
            }
        }
        return ileP;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        int minWartosc = -999;
        int maxWartosc = 999;
        generuj(tab, n, minWartosc, maxWartosc);
        System.out.println("Liczb Nieparzystych jest: "+ileNieparzystych(tab));
        System.out.println("Liczb Parzystych jest: "+ileParzystych(tab));
    }
}
