import java.util.Scanner;
import java.util.Random;

public class zadanie1_A {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        int sumP = 0;
        int sumNP = 0;
        Random number = new Random();
        for(int i=0; i<n; i++){
            tab[i] = number.nextInt(999 - (-999) + 1) + (-999);
            if(tab[i]%2==0){
                sumP+=1;
            }
            else{
                sumNP+=1;
            }
        }
        System.out.println("Liczb parzystych jest: "+sumP);
        System.out.println("Liczb nieparzystych jest: "+sumNP);
    }
}
