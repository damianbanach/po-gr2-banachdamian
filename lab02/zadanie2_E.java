import java.util.Scanner;
import java.util.Random;

public class zadanie2_E {
    public static void generuj(int tab[], int n, int minWartosc, int maxWartosc){
        Random number = new Random();
        for(int i=0; i < tab.length; i++){
            tab[i] = number.nextInt(maxWartosc - (minWartosc)+1) + (minWartosc);
        }
    }
    public static int dlugoscMaksymalnegoCiaguDodatnich(int tab[]){
        int suma = 0;
        int max = 0;
        for(int i=0; i < tab.length; i++){
            if(tab[i]>0){
                suma+=1;
            }
            else{
                if(max < suma){
                    max = suma;
                }
                suma=0;
            }
        }
        return max;
    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        int minWartosc = -999;
        int maxWartosc = 999;
        generuj(tab, n, minWartosc, maxWartosc);
        System.out.println("Dlugosc najdluzszego fragmentu tablicy, w ktorym elementy sa dodatnie wynosi: "+dlugoscMaksymalnegoCiaguDodatnich(tab));
    }
}