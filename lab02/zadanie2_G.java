import java.util.Scanner;
import java.util.Random;

public class zadanie2_G {
    public static void generuj (int tab[], int n, int minWartosc, int maxWartosc) {
        Random number = new Random();
        for (int i = 0; i < tab.length; i++)
            tab[i] = number.nextInt(maxWartosc - (minWartosc) + 1) + (minWartosc);
    }
    public static void odwrocFragment (int tab[], int lewy, int prawy) {
        int tab2[] = new int[prawy - lewy + 1];
        System.arraycopy(tab, lewy - 1, tab2, 0, tab2.length);
        for(int i = 0; i < tab2.length / 2; i++) {
            int temp = tab2[i];
            tab2[i] = tab2[tab2.length - 1 - i];
            tab2[tab2.length - 1 - i] = temp;
        }
        System.arraycopy(tab2, 0, tab, lewy - 1, tab2.length);
    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int lewy = 0;
        do {
            lewy = scan.nextInt();
        }while(0 > lewy || lewy > n);
        int prawy = 0;
        do {
            prawy = scan.nextInt();
        }while(0 > prawy || prawy > n);
        int tab[] = new int[n];
        int minWartosc = -999;
        int maxWartosc = 999;
        generuj(tab, n, minWartosc, maxWartosc);
        odwrocFragment(tab, lewy, prawy);
    }
}

