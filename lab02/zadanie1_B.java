import java.util.Random;
import java.util.Scanner;

import java.util.Scanner;
import java.util.Random;

public class zadanie1_B {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb ma byc w tablicy?");
        int n = 0;
        do {
            n = scan.nextInt();
        }while(n < 1 || 100 < n);
        int []tab = new int[n];
        int ileD = 0;
        int ileU = 0;
        int ileZ = 0;
        Random number = new Random();
        for(int i=0; i<n; i++){
            tab[i] = number.nextInt(999 - (-999) + 1) + (-999);
            if(tab[i]>0){
                ileD+=1;
            }
            else if(tab[i]==0){
                ileZ+=1;
            }
            else{
                ileU+=1;
            }
        }
        System.out.println("Liczb dodatnich jest: "+ileD);
        System.out.println("Liczb ujemnych jest: "+ileU);
        System.out.println("Zer jest: "+ileZ);
    }
}
