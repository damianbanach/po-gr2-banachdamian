import java.util.ArrayList;
import java.util.Arrays;

public class zadanie2 {
    public static ArrayList<Integer> merge(ArrayList<Integer> a, ArrayList<Integer> b) {
        ArrayList<Integer> lista = new ArrayList<>();
        int roz;
        if(a.size() <= b.size())
            roz = a.size();
        else
            roz = b.size();
        for(int i = 0; i < roz; ++i) {
            lista.add(a.get(i));
            lista.add(b.get(i));
        }
        if(a.size() < b.size())
            for(int i = roz; i < b.size(); ++i)
                lista.add(b.get(i));
        if(b.size() < a.size())
            for(int i = roz; i < a.size(); ++i)
                lista.add(a.get(i));
        int n = lista.size();
        for(int i = 0; i < n; ++i) {
            boolean pow = false;
            for(int j = 0; j < i && !pow; ++j) {
                if (lista.get(i).equals(lista.get(j))) {
                    pow = true;
                }
            }
            if(pow) {
                lista.remove(i);
                --n;
                --i;
            }
        }
        return lista;
    }
    public static void main(String[] args) {
        ArrayList<Integer> listaA = new ArrayList<>(Arrays.asList(1, 5, 5, 7, 8, 9, 9));
        ArrayList<Integer> listaB = new ArrayList<>(Arrays.asList(9, 2, 3, 4, 55, 69, 32));
        System.out.println("Lista A: "+listaA);
        System.out.println("Lista B: "+listaB);
        System.out.println("Polaczona lista skladajaca sie z listyA i listyB po usunieciu duplikatow : "+merge(listaA, listaB));
    }
}

