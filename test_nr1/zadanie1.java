import java.util.Scanner;

public class zadanie1 {
    public static int liczbaPierwsza(double liczba){
        if (liczba <= 1)
            return 0;
        else if (liczba == 2)
            return 1;
        else if (liczba % 2 == 0)
            return 0;
        for (int i = 3; i <= Math.sqrt(liczba); i += 2)
        {
            if (liczba % i == 0)
                return 0;
        }
        return 1;
    }

    public static void main(String [] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Podaj liczbe n: ");
        int n = scan.nextInt();
        double[] tab = new double[n];
        System.out.println("Podaj liczby:");
        for(int i=0;i<n;i++){
            tab[i]=scan.nextDouble();
        }
        for(int i=0; i<n; i++){
            if(liczbaPierwsza(tab[i])==0){
                System.out.println(tab[i]+" Ta liczba nie jest liczba pierwsza");
            }
            else{
                System.out.println(tab[i]+" Ta liczba jest liczba pierwsza");
            }
        }

    }
}