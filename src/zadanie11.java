package pl.edu.uwm.wmii.banachdamian.laboratorium00;

public class zadanie11 {
    public static void main(String[] args){
        String name = """
                Na szlak moich blizn poprowadź palec, 
                By nasze drogi spleść gwiazdom na przekór. 
                Otwórz te rany, a potem zalecz, 
                Aż w zawiły losu ułożą się wzór. 
                Z moich snów uciekasz nad ranem, 
                Cierpka jak agrest, słodka jak bez. 
                Chcę śnić czarne loki splątane, 
                Fiołkowe oczy mokre od łez. 
                Za wilczym śladem podążę w zamieć 
                I twoje serce wytropię uparte, 
                Przez gniew i smutek, stwardniałe w kamień 
                Rozpalę usta smagane wiatrem. 
                Z moich snów uciekasz nad ranem, 
                Cierpka jak agrest, słodka jak bez. 
                Chcę śnić czarne loki splątane, 
                Fiołkowe oczy mokre od łez. 
                Nie wiem, czy jesteś moim przeznaczeniem, 
                Czy przez ślepy traf miłość nas związała. 
                Kiedy wyrzekłem moje życzenie, 
                Czyś mnie wbrew sobie wtedy pokochała? 
                Z moich snów uciekasz nad ranem, 
                Cierpka jak agrest, słodka jak bez. 
                Chcę śnić czarne loki splątane, 
                Fiołkowe oczy mokre od łez.
                                
         
                           """;
        System.out.println(name);

    }

}