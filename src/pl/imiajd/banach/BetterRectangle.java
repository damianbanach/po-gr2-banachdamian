package pl.imiajd.banach;
import java.awt.Rectangle;

public class BetterRectangle extends Rectangle {

    public BetterRectangle(int x, int y, int szerokosc, int wysokosc) {
        super(x, y, szerokosc, wysokosc);
    }

    public int getPerimeter() {
        return 2*this.width + 2*this.height;
    }

    public int getArea() {
        return this.width*this.height;
    }
}
