package pl.imiajd.banach;

public class Adres {

    public Adres(String ulica, String numer_domu, String numer_mieszkania) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
    }

    public Adres(String ulica, String numer_domu, String numer_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz() {
        System.out.println(this.kod_pocztowy+" "+this.miasto);
        System.out.println(this.ulica+" "+this.numer_domu+"/"+this.numer_mieszkania);
    }

    public boolean przed(Adres adres) {
        for(int i = 0; i < this.kod_pocztowy.length(); ++i)
        {
            if(this.kod_pocztowy.charAt(i) < adres.kod_pocztowy.charAt(i))
                return true;
            if(this.kod_pocztowy.charAt(i) > adres.kod_pocztowy.charAt(i))
                return false;
        }
        return false;
    }

    private String ulica, numer_domu, numer_mieszkania, miasto, kod_pocztowy;
}