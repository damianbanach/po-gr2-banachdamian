package pl.imiajd.banach;

public class Osoba {

    public Osoba(String nazwisko, String rokUrodzenia) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String toString() {
        return this.nazwisko+" "+this.rokUrodzenia;
    }

    public String getNazwisko() {
        return this.nazwisko;
    }

    public String getRokUrodzenia() {
        return this.rokUrodzenia;
    }

    private String nazwisko, rokUrodzenia;
}
