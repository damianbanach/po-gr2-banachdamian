package pl.imiajd.banach;

public class Nauczyciel extends Osoba {

    public Nauczyciel(String nazwisko, String rokUrodzenia, String pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public String toString() {
        return super.toString()+" "+this.pensja;
    }

    public String getPensja(){
        return this.pensja;
    }

    private String pensja;
}
