package pl.imiajd.banach;

public class Student extends Osoba {

    public Student(String nazwisko, String rokUrodzenia, String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String toString() {
        return super.toString()+" "+this.kierunek;
    }

    public String getKierunek() {
        return this.kierunek;
    }

    private String kierunek;
}
