import java.util.HashSet;
import java.util.Scanner;

public class zadanie7 {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        HashSet<Integer> pierwsze = new HashSet<>();
        System.out.print("Podaj n: ");
        int n = scan.nextInt();
        for(int i = 2; i <= n; ++i) {
            pierwsze.add(i);
        }
        for(int i = 2; i <= Math.sqrt(n); ++i) {
            if(pierwsze.contains(i)) {
                for (int j = 2; j*i <= n; ++j) {
                    pierwsze.remove(j*i);
                }
            }
        }
        System.out.println(pierwsze);
    }
}
