import java.util.Scanner;
import java.util.Stack;

public class zadanie6 {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Stack<Integer> stos = new Stack<>();
        int liczba;
        do {
            System.out.print("Podaj liczbe naturalna: ");
            liczba = scan.nextInt();
        } while(liczba < 0);
        if(liczba == 0)
            stos.push(liczba);
        else {
            while (liczba != 0) {
                stos.push(liczba % 10);
                liczba /= 10;
            }
        }
        while(!stos.isEmpty()) {
            Integer a = stos.pop();
            if(stos.isEmpty())
                System.out.print(a);
            else
                System.out.print(a+" ");
        }
    }
}
