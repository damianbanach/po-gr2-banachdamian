import java.util.LinkedList;
public class zadanie2 {
    public static void main(String[] args){
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Donald Trump");
        pracownicy.add("Barack Obama");
        pracownicy.add("John Kennedy");
        pracownicy.add("Joe Biden");
        pracownicy.add("Bill Clinton");
        pracownicy.add("Jimmy Carter");
        pracownicy.add("Richard Nixon");
        pracownicy.add("Harry Truman");
        pracownicy.add("Dwight Eisenhower");
        System.out.println("Oryginalna LinkedList:");
        System.out.println(pracownicy);
        System.out.println();
        redukuj(pracownicy, 3);
        System.out.println("Modyfikowana LinkedList:");
        System.out.println(pracownicy);
    }
    public static <T> void redukuj(LinkedList<T> pracownicy, int n){
        for(int i = n-1; i < pracownicy.size(); i = i+n){
            pracownicy.remove(i);
            --i;
        }
    }
}
