import java.util.LinkedList;

public class zadanie4 {
    public static void main(String[] args){
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Donald Trump");
        pracownicy.add("Barack Obama");
        pracownicy.add("John Kennedy");
        pracownicy.add("Joe Biden");
        pracownicy.add("Bill Clinton");
        pracownicy.add("Jimmy Carter");
        pracownicy.add("Richard Nixon");
        pracownicy.add("Harry Truman");
        pracownicy.add("Dwight Eisenhower");
        System.out.println("Oryginalna LinkedList:");
        System.out.println(pracownicy);
        System.out.println();
        odwroc(pracownicy);
        System.out.println("Modyfikowana LinkedList:");
        System.out.println(pracownicy);
    }
    public static <T> void odwroc(LinkedList<T> lista) {
        int rozmiar = lista.size();
        for(int i = 0; i < rozmiar/2; ++i) {
            T temp = lista.get(rozmiar-1-i);
            lista.set(rozmiar-1-i, lista.get(i));
            lista.set(i, temp);
        }
    }
}
