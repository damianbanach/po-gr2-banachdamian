import java.util.LinkedList;
import java.util.Iterator;

public class zadanie8 {
    public static void main(String[] args){
        LinkedList<String> pracownicy = new LinkedList<>();
        pracownicy.add("Donald Trump");
        pracownicy.add("Barack Obama");
        pracownicy.add("John Kennedy");
        pracownicy.add("Joe Biden");
        pracownicy.add("Bill Clinton");
        pracownicy.add("Jimmy Carter");
        pracownicy.add("Richard Nixon");
        pracownicy.add("Harry Truman");
        pracownicy.add("Dwight Eisenhower");
        print(pracownicy);
        System.out.println("\n");
        System.out.println(pracownicy);
    }
    public static <T extends Iterable> void print(T obj) {
        Iterator iterator = obj.iterator();
        System.out.print(iterator.next());
        while (iterator.hasNext()) {
            System.out.print("\n"+iterator.next());
        }
    }
}