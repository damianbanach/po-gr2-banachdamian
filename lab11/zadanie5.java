import java.util.*;

public class zadanie5{
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.print("Wpisz zdania: ");
        String tekst = scan.nextLine();
        StringBuffer string = new StringBuffer().append(tekst);
        int ile_spacji = 0;
        for(int i = 0; i < string.length(); ++i) {
            if(string.charAt(i) == ' ')
                ++ile_spacji;
            else
                ile_spacji = 0;
            if(ile_spacji > 1) {
                string.deleteCharAt(i);
                --i;
            }
        }
        tekst = string.toString();
        List<String> wyrazy = Arrays.asList(tekst.split(" "));
        Stack<String> stos = new Stack<>();
        for(int i = 0; i < wyrazy.size(); ++i) {
            stos.push(wyrazy.get(i));
            if(wyrazy.get(i).contains(".")) {
                String wyraz = stos.pop();
                System.out.print(wyraz.substring(0, 1).toUpperCase()+wyraz.substring(1, wyraz.length()-1)+" ");
                while (!stos.isEmpty()) {
                    wyraz = stos.pop();
                    if(!stos.isEmpty())
                        System.out.print(wyraz.toLowerCase()+" ");
                    else
                    if(i != wyrazy.size()-1)
                        System.out.print(wyraz.toLowerCase()+". ");
                    else
                        System.out.print(wyraz.toLowerCase()+".");
                }
            }
        }
    }
}
