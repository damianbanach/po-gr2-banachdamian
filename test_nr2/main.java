import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;

public class main {

    public static void main(String[] args) {
        ArrayList<Klient> customers = new ArrayList<>();
        customers.add(new Klient("Joe Biden", 1, 2020, 7, 7, 0));
        customers.add(new Klient("Donald Trump", 2, 2020, 7, 7, 0));
        customers.add(new Klient("Ronald Reagan", 3, 1979, 12, 24, 5000));
        customers.add(new Klient("Bill Clinton", 4, 2012, 4, 11, 110));
        customers.add(new Klient("Barack Obama", 5, 2005, 5, 5, 5000));
        customers.add(new Klient("Franklin Clinton", 6, 2013, 9, 17, 7000));
        Obslugaklienta klienci = new Obslugaklienta(customers);
        System.out.println("Klienci w bazie:");
        for(Klient klient : klienci.getKlienci())
            System.out.println(klient);

        customers = klienci.getKlienci();
        Collections.sort(customers);
        klienci.setKlienci(customers);

        System.out.println("\n");
        System.out.println("Posortowana baza klientów wg daty zakupy, nazwy oraz rachunku:");
        for(Klient klient : klienci.getKlienci())
            System.out.println(klient);

        System.out.println("\n");
        System.out.println("Rabaty klientow:");
        Obslugaklienta.setProcentRabatu();
        for(Klient klient : klienci.getKlienci())
            System.out.println(klient+", "+"Rabat --> "+discountAmount(klient));
    }

    public static double discountAmount(Klient k) {
        if(k.getRachunek() > 300)
            return k.getRachunek() * Obslugaklienta.getProcentRabatu();
        return 0;
    }
}

class Klient implements Cloneable, Comparable<Klient> {

    private String nazwa;
    private int id;
    private LocalDate dataZakupy;
    private double rachunek;

    public Klient(String nazwa, int id, int rok, int miesiac, int dzien, double rachunek) {
        this.nazwa = nazwa;
        this.id = id;
        this.dataZakupy = LocalDate.of(rok, miesiac, dzien);
        this.rachunek = rachunek;
    }

    public String getNazwa() {
        return nazwa;
    }

    public int getId() {
        return id;
    }

    public LocalDate getDataZakupu() {
        return dataZakupy;
    }

    public double getRachunek() {
        return rachunek;
    }

    @Override
    public int compareTo(Klient o) {
        int result = this.dataZakupy.compareTo(o.getDataZakupu());
        if(result != 0)
            return result;
        result = this.nazwa.compareTo(o.getNazwa());
        if(result != 0)
            return result;
        if(this.rachunek < o.getRachunek())
            return -1;
        if(this.rachunek > o.getRachunek())
            return 1;
        return 0;
    }

    @Override
    public String toString() {
        return this.nazwa +", "+ this.id +", "+ this.dataZakupy +", "+ this.rachunek;
    }
}

class Obslugaklienta {

    private static double procentRabatu;
    private ArrayList<Klient> klienci;

    public Obslugaklienta(ArrayList<Klient> klienci) {
        this.klienci = klienci;
    }

    public static double getProcentRabatu() {
        return procentRabatu;
    }

    public ArrayList<Klient> getKlienci() {
        return klienci;
    }

    public static void setProcentRabatu() {
        Obslugaklienta.procentRabatu = 0.05;
    }

    public void setKlienci(ArrayList<Klient> klienci) {
        this.klienci = klienci;
    }
}

