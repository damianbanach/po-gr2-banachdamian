package pl.edu.uwm.wmii.banachdamian.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;

public class zadanie2_1e {
    public static int silnia(int n) {
        if (n == 0) {
            return 1;
        } else {
            return (n * silnia(n - 1));
        }

    }

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb:");
        int liczba = scan.nextInt();
        int ile = 0;
        int tab[] = new int[liczba];
        System.out.println("Podaj liczby:");
        for (int i = 0; i < liczba; i++) {
            tab[i] = scan.nextInt();
        }
        for(int j=0;j<liczba;j++) {

            if (j > 0 && j < liczba - 1) {
                if (tab[j] > (Math.pow(2, j + 1)) && tab[j] < silnia(j)) {
                    ile++;
                }
            }
        }
        System.out.println("Liczb spelniajacych warunek podany w zadaniu jest: " + ile);
    }
}
