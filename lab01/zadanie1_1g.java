package pl.edu.uwm.wmii.banachdamian.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;

public class zadanie1_1g {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb:");
        int liczba = scan.nextInt();
        double suma=0;
        double iloczyn=1;
        double sumaLaczna=0;
        double tab[] = new double[liczba];
        System.out.println("Podaj liczby:");
        for(int i=0; i<liczba; i++){
            tab[i] = scan.nextInt();
        }
        for(int i=0;i<liczba;i++){
            suma+=tab[i];
        }
        for(int i=0;i<liczba;i++){
            iloczyn*=tab[i];
        }
        sumaLaczna=suma+iloczyn;
        System.out.println("Suma wynosi: "+sumaLaczna);
    }
}