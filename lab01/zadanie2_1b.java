package pl.edu.uwm.wmii.banachdamian.laboratorium01;

import java.util.Scanner;

public class zadanie2_1b {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb:");
        int liczba = scan.nextInt();
        int ile=0;
        int tab[] = new int[liczba];
        System.out.println("Podaj liczby:");
        for(int i=0; i<liczba; i++){
            tab[i] = scan.nextInt();
        }
        for(int i=0;i<liczba;i++){
            if(tab[i]%3==0 && !(tab[i]%5==0))
            {
                ile++;
            }
        }
        System.out.println("Liczb podzielnych przez 3, ale niepodzielnych przez 5 jest: "+ile);
    }
}