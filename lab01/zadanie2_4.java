package pl.edu.uwm.wmii.banachdamian.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;

public class zadanie2_4 {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb:");
        int liczba = scan.nextInt();
        int max=0;
        int min=0;
        int tab[] = new int[liczba];
        System.out.println("Podaj liczby:");
        for(int i=0; i<liczba; i++){
            tab[i] = scan.nextInt();
        }
        max=tab[0];
        for(int i=0;i<liczba;i++){
            if(max<=tab[i]){
                max=tab[i];
            }
        }
        min=tab[0];
        for(int i=0;i<liczba;i++){
            if(min>=tab[i]){
                min=tab[i];
            }
        }
        System.out.println("Najwieksza liczba w tym zbiorze: "+max+"\nNajmniejsza liczba w zbiorze: "+min);
    }
}
