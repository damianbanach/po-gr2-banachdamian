package pl.edu.uwm.wmii.banachdamian.laboratorium01;

import java.util.Scanner;
import static java.lang.Math.*;

public class zadanie1_1i {
    public static int silnia(int n) {
        if (n==0){
            return 1;
        }

        else{
            return (n*silnia(n-1));
        }

    }
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb:");
        int liczba = scan.nextInt();
        double wynik=0;
        int tab[] = new int[liczba];
        System.out.println("Podaj liczby:");
        for(int i=0; i<liczba; i++){
            tab[i] = scan.nextInt();
        }
        for(int i=0;i<liczba;i++){
            wynik += (pow(-1,i)*tab[i])/silnia(i);
        }
        System.out.println("Suma wynosi: "+wynik);
    }
}