package pl.edu.uwm.wmii.banachdamian.laboratorium01;

import java.util.Scanner;

public class zadanie1_1b {
    public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        System.out.println("Ile liczb:");
        int liczba = scan.nextInt();
        double iloczyn=1;
        int tab[] = new int[liczba];
        System.out.println("Podaj liczby:");
        for(int i=0; i<liczba; i++){
            tab[i] = scan.nextInt();
        }
        for(int i=0;i<liczba;i++){
            iloczyn*=tab[i];
        }
        System.out.println("Suma wynosi: "+iloczyn);
    }
}