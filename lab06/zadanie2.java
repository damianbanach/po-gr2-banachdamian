public class zadanie2{
    public static void main(String[] args){
        IntegerSet a=new IntegerSet();
        IntegerSet b=new IntegerSet();
        a.insertElement(1);
        a.insertElement(2);
        a.insertElement(3);
        b.insertElement(3);
        b.insertElement(4);
        b.insertElement(5);

        System.out.println("a: "+a.toString());
        System.out.println("b: "+b.toString());
        System.out.println("union: "+IntegerSet.union(a, b).toString());
        System.out.println("intersection: "+IntegerSet.intersection(a, b).toString());
        System.out.println("intersection: "+IntegerSet.intersection(a, b).toString());
        System.out.println("equals: "+a.equals(b));

        b.insertElement(1);
        b.insertElement(2);
        b.deleteElement(4);
        b.deleteElement(5);

        System.out.println("a: "+a.toString());
        System.out.println("b: "+b.toString());
        System.out.println("equals: "+a.equals(b));
    }
}

class IntegerSet{
    private boolean[] liczby;

    public static IntegerSet union(IntegerSet a, IntegerSet b){
        IntegerSet c=new IntegerSet();

        for(int i=0; i<100; i++)
            if(a.liczby[i] || b.liczby[i]) c.liczby[i]=true;

        return c;
    }

    public static IntegerSet intersection(IntegerSet a, IntegerSet b){
        IntegerSet c=new IntegerSet();

        for(int i=0; i<100; i++)
            if(a.liczby[i] && b.liczby[i]) c.liczby[i]=true;

        return c;
    }

    public void insertElement(int i){
        this.liczby[i-1]=true;
    }

    public void deleteElement(int i){
        this.liczby[i-1]=false;
    }

    public String toString(){
        String w="";

        for(int i=0; i<100; i++)
            if(this.liczby[i]) w+=(i+1)+" ";

        return w;
    }

    public boolean equals(IntegerSet a){
        for(int i=0; i<100; i++)
            if(this.liczby[i]!=a.liczby[i])
                return false;
        return true;
    }

    IntegerSet(){
        liczby=new boolean[100];
    }
}
