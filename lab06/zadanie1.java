public class zadanie1 {
    public static class RachunekBankowy{
        static double RocznaStopaProcentowa;
        private double saldo;
        double obliczMiesieczneOdsetki(){
            double odsetki;
            odsetki = (saldo*RocznaStopaProcentowa)/12;
            saldo+=odsetki;
            return saldo;
        }
        void setRocznaStopaProcentowa(){
            RocznaStopaProcentowa = 0.05;
        }
    }
    public static void main(String[] args){
        RachunekBankowy saver1 = new RachunekBankowy();
        RachunekBankowy saver2 = new RachunekBankowy();
        saver1.saldo = 2000;
        saver1.RocznaStopaProcentowa = 0.04;
        System.out.println("Stan salda saver1 z roczna stopa procentowa 0.04%: "+saver1.obliczMiesieczneOdsetki());
        saver1.setRocznaStopaProcentowa();
        System.out.println("Stan salda saver1 z roczna stopa procentowa 0.05%: "+saver1.obliczMiesieczneOdsetki());
        saver2.saldo = 3000;
        saver2.RocznaStopaProcentowa = 0.04;
        System.out.println("Stan salda saver2 z roczna stopa procentowa 0.04%: "+saver2.obliczMiesieczneOdsetki());
        saver2.setRocznaStopaProcentowa();
        System.out.println("Stan salda saver2 z roczna stopa procentowa 0.05%: "+saver2.obliczMiesieczneOdsetki());
    }
}
