public class zadanie2 {

    public static void main(String[] args) {
        Adres Adres1 = new Adres("Watykanska", "109", "15", "Warszawa", "07-920");
        Adres Adres2 = new Adres("Warszawska", "23", "11", "Olsztyn", "10-001");
        System.out.println("Adres numer1:");
        Adres1.pokaz();
        System.out.println("\nAdres Numer2:");
        Adres2.pokaz();
        System.out.println("\nAdres1.przed(adres2) = "+Adres1.przed(Adres2));
        System.out.println("adres2.przed(adres1) = "+Adres2.przed(Adres1));
    }
}

class Adres {
    private String ulica, numer_domu, numer_mieszkania, miasto, kod_pocztowy;
    public Adres(String ulica, String numer_domu, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public Adres(String ulica, String numer_domu, String numer_mieszkania, String miasto, String kod_pocztowy) {
        this.ulica = ulica;
        this.numer_domu = numer_domu;
        this.numer_mieszkania = numer_mieszkania;
        this.miasto = miasto;
        this.kod_pocztowy = kod_pocztowy;
    }

    public void pokaz() {
        System.out.println(this.kod_pocztowy+" "+this.miasto);
        System.out.println(this.ulica+" "+this.numer_domu+"/"+this.numer_mieszkania);
    }

    public boolean przed(Adres adres) {
        for(int i = 0; i < this.kod_pocztowy.length(); ++i)
        {
            if(this.kod_pocztowy.charAt(i) < adres.kod_pocztowy.charAt(i))
                return true;
            if(this.kod_pocztowy.charAt(i) > adres.kod_pocztowy.charAt(i))
                return false;
        }
        return false;
    }
}