import pl.imiajd.banach.Osoba;
import pl.imiajd.banach.Student;
import pl.imiajd.banach.Nauczyciel;

public class zadanie5 {

    public static void main(String[] args) {
        Osoba osoba = new Osoba("Geralt", "1190");
        Student student = new Student("Lambert", "1189", "Wiedzminologia");
        Nauczyciel nauczyciel = new Nauczyciel("Vesemir", "1090", "1000 KORON");

        System.out.println("Osoba:");
        System.out.println("   Nazwisko - "+osoba.getNazwisko());
        System.out.println("   Rok Urodzenia - "+osoba.getRokUrodzenia());
        System.out.println();
        System.out.println("Student:");
        System.out.println("   Nazwisko - "+student.getNazwisko());
        System.out.println("   Rok Urodzenia - "+student.getRokUrodzenia());
        System.out.println("   Kierunek - "+student.getKierunek());
        System.out.println();
        System.out.println("Nauczyciel:");
        System.out.println("   Nazwisko - "+nauczyciel.getNazwisko());
        System.out.println("   Rok Urodzenia - "+nauczyciel.getRokUrodzenia());
        System.out.println("   Pensja - "+nauczyciel.getPensja());
    }
}