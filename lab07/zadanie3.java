import pl.imiajd.banach.Adres;

public class zadanie3 {

    public static void main(String[] args) {
        Adres Adres1 = new Adres("Watykanska", "109", "15", "Warszawa", "07-920");
        Adres Adres2 = new Adres("Warszawska", "23", "11", "Olsztyn", "10-001");
        System.out.println("Adres numer1:");
        Adres1.pokaz();
        System.out.println("\nAdres Numer2:");
        Adres2.pokaz();
        System.out.println("\nAdres1 przed = " + Adres1.przed(Adres2));
        System.out.println("Adres2 przed = " + Adres2.przed(Adres1));
    }
}