import java.awt.Rectangle;

public class zadanie7 {
    public static void main(String[] args) {
        int x = 0, y = 0, szerokosc = 10, wysokosc= 30;
        BetterRectangle rectangle = new BetterRectangle(x, y, szerokosc, wysokosc);
        System.out.println("Szerokość = "+szerokosc);
        System.out.println("Wysokość = "+wysokosc);
        System.out.println("Obwod prostokata = "+rectangle.getPerimeter());
        System.out.println("Pole prostokata = "+rectangle.getArea());
    }
}
class BetterRectangleSuper extends Rectangle {

    public BetterRectangleSuper(int x, int y, int szerokosc, int wysokosc) {
        super(x, y, szerokosc, wysokosc);
    }

    public int getPerimeter() {
        return 2*this.width + 2*this.height;
    }

    public int getArea() {
        return this.width*this.height;
    }
}
