public class zadanie4{

    public static void main(String[] args) {
        Osoba osoba = new Osoba("Geralt", "1190");
        Student student = new Student("Lambert", "1189", "Wiedzminologia");
        Nauczyciel nauczyciel = new Nauczyciel("Vesemir", "1090", "1000 KORON");

        System.out.println("Osoba:");
        System.out.println("   Nazwisko - "+osoba.getNazwisko());
        System.out.println("   Rok Urodzenia - "+osoba.getRokUrodzenia());
        System.out.println();
        System.out.println("Student:");
        System.out.println("   Nazwisko - "+student.getNazwisko());
        System.out.println("   Rok Urodzenia - "+student.getRokUrodzenia());
        System.out.println("   Kierunek - "+student.getKierunek());
        System.out.println();
        System.out.println("Nauczyciel:");
        System.out.println("   Nazwisko - "+nauczyciel.getNazwisko());
        System.out.println("   Rok Urodzenia - "+nauczyciel.getRokUrodzenia());
        System.out.println("   Pensja - "+nauczyciel.getPensja());
    }
}

class Osoba {

    public Osoba(String nazwisko, String rokUrodzenia) {
        this.nazwisko = nazwisko;
        this.rokUrodzenia = rokUrodzenia;
    }

    public String toString() {
        return this.nazwisko+" "+this.rokUrodzenia;
    }

    public String getNazwisko() {
        return this.nazwisko;
    }

    public String getRokUrodzenia() {
        return this.rokUrodzenia;
    }

    private String nazwisko, rokUrodzenia;
}

class Student extends Osoba {

    public Student(String nazwisko, String rokUrodzenia, String kierunek) {
        super(nazwisko, rokUrodzenia);
        this.kierunek = kierunek;
    }

    public String toString() {
        return super.toString()+" "+this.kierunek;
    }

    public String getKierunek() {
        return this.kierunek;
    }

    private String kierunek;
}

class Nauczyciel extends Osoba {

    public Nauczyciel(String nazwisko, String rokUrodzenia, String pensja) {
        super(nazwisko, rokUrodzenia);
        this.pensja = pensja;
    }

    public String toString() {
        return super.toString()+" "+this.pensja;
    }

    public String getPensja(){
        return this.pensja;
    }

    private String pensja;
}
