import java.lang.Integer;
import java.time.LocalDate;
import java.util.Random;

public class ArrayUtilTest {
    public static void main(String[] args){
        Integer[] tab1=new Integer[5];
        Integer[] tab2=new Integer[5];
        LocalDate[] tab3=new LocalDate[5];
        LocalDate[] tab4=new LocalDate[5];

        for(int i=0; i<5; i++)
            tab1[i]=Integer.valueOf(5-i);
        for(int i=0; i<5; i++)
            tab2[i]=Integer.valueOf(new Random().nextInt(10)+1);
        for(int i=0; i<5; i++)
            tab3[i]=LocalDate.of(1998, 9, 17-i);
        for(int i=0; i<5; i++)
            tab4[i]=LocalDate.of(new Random().nextInt(80)+1941, new Random().nextInt(12)+1, new Random().nextInt(28)+1);

        System.out.print("\nTablica 1: ");
        for(int i=0; i<5; i++)
            System.out.print(tab1[i]+", ");
        System.out.print("\nTablica 2: ");
        for(int i=0; i<5; i++)
            System.out.print(tab2[i]+", ");
        System.out.print("\nTablica 3: ");
        for(int i=0; i<5; i++)
            System.out.print(tab3[i].toString()+", ");
        System.out.print("\nTablica 4: ");
        for(int i=0; i<5; i++)
            System.out.print(tab4[i].toString()+", ");

        System.out.println("\nTablica 1: "+ArrayUtil.isSorted(tab1));
        System.out.println("Tablica 2: "+ArrayUtil.isSorted(tab2));
        System.out.println("Tablica 3: "+ArrayUtil.isSorted(tab3));
        System.out.println("Tablica 4: "+ArrayUtil.isSorted(tab4));
        System.out.println();
        ArrayUtil.mergeSort(tab1);
        System.out.print("Tablica 1 posortowane: ");
        for(int i=0; i<5; i++)
            System.out.print(tab1[i]+", ");
        System.out.println("\n2 w Tablicy 1: "+ArrayUtil.binSearch(tab1, 2));
        System.out.println("4 w Tablica 1: "+ArrayUtil.binSearch(tab1, 4));
        System.out.println("11 w Tablica 1: "+ArrayUtil.binSearch(tab1, 11));
        System.out.println();
        ArrayUtil.mergeSort(tab3);
        System.out.print("Tablica 3 posortowane: ");
        for(int i=0; i<5; i++)
            System.out.print(tab3[i].toString()+", ");
        System.out.println("\n1998-09-17 w tab3: "+ArrayUtil.binSearch(tab3, LocalDate.of(1998, 9, 17)));
        System.out.println("1998-09-13 w tab3: "+ArrayUtil.binSearch(tab3, LocalDate.of(1998, 9, 13)));
        System.out.println("1998-10-23 w tab3: "+ArrayUtil.binSearch(tab3, LocalDate.of(1998, 10, 23)));
    }
}
