public class PairUtilDemo {
    public static void main(String[] args)
    {
        Pair<String> para=new Pair<String>("Daniel", "Konrad");

        System.out.println(para.getFirst());
        System.out.println(para.getSecond());
        Pair<String>p=PairUtil.swap(para);
        System.out.println(p.getFirst());
        System.out.println(p.getSecond());
    }
}