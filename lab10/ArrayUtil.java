public class ArrayUtil<T>{
    public static <T extends Comparable<T>>boolean isSorted(T[] tab){
        for(int i=0; i<tab.length-1; i++){
            if(tab[i].compareTo(tab[i+1])<0)
                return false;
        }
        return true;
    }

    public static <T extends Comparable<T>>int binSearch(T[] tab, T x){
        int lewo=0;
        int prawo=tab.length-1;
        int i;

        while(lewo<=prawo){
            i=(lewo+prawo)/2;
            if(tab[i].compareTo(x)<0)
                lewo=i+1;
            else if(tab[i].compareTo(x)>0)
                prawo=i-1;
            else
                return i;
        }

        return -1;
    }

    public static <T extends Comparable<T>>void selectionSort(T[] tab){
        T min;
        int minIndeks;
        for(int i=0; i<tab.length-1; i++){
            min=tab[i];
            minIndeks=i;
            for(int j=i; j<tab.length; j++)
                if(tab[j].compareTo(min)<0){
                    min=tab[j];
                    minIndeks=j;
                }
            tab[minIndeks]=tab[i];
            tab[i]=min;
        }
    }

    private static <T extends Comparable<T>>void merge(T[] tab, int l1, int l2, int prawa){
        int k=0, lp=l2;
        T[] tp=tab.clone();

        while(l1<lp&&l2<prawa)
            tp[k++]=(tab[l1].compareTo(tab[l2])<0)?tab[l1++]:tab[l2++];
        while(l1<lp) tp[k++]=tab[l1++];
        while(l2<prawa) tp[k++]=tab[l2++];
        while(k>0) tab[--prawa]=tp[--k];
    }

    public static <T extends Comparable<T>>void mergeSort(T[] tab){
        int k;
        for(int i=1; i<tab.length; i+=i)
            for(int j=0; j+i<tab.length; j+=2*i){
                k=(j+2*i<tab.length)? j+2*i:tab.length;
                merge(tab, j, j+i, k);
            }
    }
}
