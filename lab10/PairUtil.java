public class PairUtil{
    public static <T>Pair<T> swap(Pair<T> para){
        Pair<T> p=new Pair<T>(para.getSecond(), para.getFirst());
        return p;
    }
}
